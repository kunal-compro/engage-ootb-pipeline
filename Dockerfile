FROM node:10.15.3
COPY package*.json /

RUN npm install
COPY pipe /
CMD ["node", "/index.js"]