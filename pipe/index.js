const fs = require("fs");
const AWS = require('aws-sdk');
const path = require("path");
// Environment Variables
const AWS_REGION = process.env.AWS_REGION;
const AWS_ACCESS = process.env.AWS_KEY;
const AWS_SECRET = process.env.AWS_SECRET;
const domain = process.env.DOMAIN;
const tableName = process.env.DYNAMODB_TABLE;
const workingDirUrl = process.env.BITBUCKET_CLONE_DIR;

// Fetch the client env json
const clientEnvJson = JSON.parse(
  fs.readFileSync(workingDirUrl + "/client_env.json")
);
const clientId = clientEnvJson.client_id;
const ootbAssetsPath = workingDirUrl + "/" + clientEnvJson.ootb.src.folder;
const ootbAssetsS3Path = clientEnvJson.env_filestore.base_path + clientEnvJson.ootb.folder_strategy.folder + clientEnvJson.ootb.revision + '/';
const bucketName = process.env.S3_BUCKET;
// Fetch the backend config 
const ootbConfigPath = workingDirUrl + "/" + clientEnvJson.ootb.src.config_json;
const ootbConfig = JSON.stringify(JSON.parse(
  fs.readFileSync(ootbConfigPath)
));

initializeAWS();
uploadDir(ootbAssetsPath, bucketName, ootbAssetsS3Path);
AWS.config.update({region: AWS_REGION});
const documentClient = initializeDynamoDB();

var params = getQueryParams();

documentClient.query(params, function (err, data) {
  if (err) {
    console.log(err);
    throw(err);
  }
  else {
    let isClientNew = data.Count ? false : true;
    updateDataForClient(isClientNew);
  };
});

/**
 * Initialize the AWS SDK
 */
function initializeAWS() {
  AWS.config.update({
    region: AWS_REGION,
    accessKeyId: AWS_ACCESS,
    secretAccessKey: AWS_SECRET
  });
}

/**
 * Initialize the Dynamo DB
 */
function initializeDynamoDB() {
  return new AWS.DynamoDB.DocumentClient();
}

/**
 * Form the parameters to query dynamoDB for client id
 */
function getQueryParams() {
  return {
    TableName: tableName,
    KeyConditionExpression: 'ID=:id',
    ExpressionAttributeValues: {
      ':id': clientId
    }
  };
}

/**
 * Update data for a specific client
 * @param {*} isItemNew 
 */
function updateDataForClient(isItemNew) {
  var timestamp = Date.now();
  var params = {
    TableName: tableName,
    Key: {
      'ID': clientId
    },
    UpdateExpression: "set last_updated_date=:last_updated_date, expapp_config=:config",
    ExpressionAttributeValues: {
      ":config": ootbConfig,
      ":last_updated_date": timestamp
    }
  };

  if (isItemNew) {
    params = updateParamsForNewClient(params, timestamp);
  }

  documentClient.update(params, function (err, data) {
    if (err) {
      console.log(err);
      throw(err);
    }
    else console.log("DynamoDB Updated");
  });
}

/**
 * Function to update params incase of new client
 * @param {*} params 
 * @param {*} timestamp 
 */
function updateParamsForNewClient(params, timestamp) {
  params.UpdateExpression += ', created_date=:created_date';
  params.ExpressionAttributeValues[":created_date"] = timestamp;
  return params;
}

function uploadDir(s3Path, bucketName, s3Key) {
    AWS.config.update({region: 'us-east-1'});
    console.log('Starting uploading files to S3');
    let s3 = new AWS.S3();

    function walkSync(currentDirPath, callback) {
        // console.log('Current directory path: '+currentDirPath);
        fs.readdirSync(currentDirPath).forEach(function (name) {
            // console.log('Uploading file: '+name);
            var filePath = path.join(currentDirPath, name);
            var stat = fs.statSync(filePath);
            if (stat.isFile()) {
                callback(filePath, stat);
            } else if (stat.isDirectory()) {
                walkSync(filePath, callback);
            }
        });
    }

    walkSync(s3Path, function(filePath, stat) {
        let bucketPath = filePath.substring(s3Path.length+1);
        let params = {Bucket: bucketName, Key: s3Key+bucketPath, Body: fs.readFileSync(filePath) };
        console.log('Uploading files to KEY: '+params.Key);
        s3.putObject(params, function(err, data) {
            if (err) {
                console.log(err)
                console.log('Failed uploading files to S3');
            } else {
                console.log('Successfully uploaded '+ bucketPath +' to ' + bucketName);
            }
        });

    });
};
